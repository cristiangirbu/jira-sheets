import gspread
from oauth2client.service_account import ServiceAccountCredentials
import requests
import json
import configparser
from bs4 import BeautifulSoup
import os

url = ''
user = ''
password = ''
search_url = ''
path = ''
email = ''
json_key = ''
jql = ''
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
spreadsheet = 'Export'

def readconfig():
    config = configparser.ConfigParser()
    config.read("./config.ini")

    global url
    url = config.get("jira", "url")
    global user
    user = config.get("jira", "user")
    global password
    password = config.get("jira", "pass")
    global search_url
    search_url = config.get("jira", "search-url")

    global jql
    jql = config.get("jira", "jql")

    global path
    path = config.get("sheets", "path")
    global email
    email = config.get("sheets", "email")
    global json_key
    json_key = config.get("sheets", "json-key")

def main():

    readconfig()
    s = requests.session()
    s.auth = (user, password)
    r = s.get(url)

    jira_search(s, jql)
    sheetsauth(path + "test.html")
    

def jira_search(session, jql):
    final_url = url + search_url + jql
    r = session.get(final_url)

    text = str(r.text.encode("utf-8"))

    file_path = path + "test.html"
    try:
        os.mkdir(path)
    except FileExistsError:
        pass

    try:
        fp = open(file_path, "wb")
    except FileNotFoundError:
        fp = open(file_path, 'wb+')

    fp.write(r.text.encode('utf-8', 'ignore'))
    fp.close()

def parse_html(file_name):
	columns=[]
	refs=[]
	values=[]
	temp=[]

	html_doc = open(file_name, "r").read()
	soup = BeautifulSoup(html_doc, 'html.parser')

	for line in soup.find_all('th'):
		columns.append(line.contents[0].strip())
		refs.append(line.get('data-id'))

	for ref in refs:
		for line in soup.find_all(attrs={"class": ref}):
			_len = len(list(line.stripped_strings))
			if _len == 0:
				temp.append("NULL")
			elif _len == 1:
				temp.append(list(line.stripped_strings)[0])
			else:
				val = ""
				for e in list(line.stripped_strings):
					val += e + " "
				val = val[:-1]
				temp.append(val)
		values.append(temp)
		temp=[]

	return (columns, values)

def sheetsauth(html_path):
    credentials = ServiceAccountCredentials.from_json_keyfile_name(json_key, scope)

    gc = gspread.authorize(credentials)
    try:
        sh = gc.open(spreadsheet)
    except gspread.exceptions.SpreadsheetNotFound:
        print("Creating new spreadsheet.")
        sh = gc.create(spreadsheet)

    sh.share(email, perm_type='group', role='writer')
    (columns,values) = parse_html(html_path)
    worksheet = sh.sheet1

    #constructing header(columns)
    _range = get_range_header(len(columns), _range='A1:')
    header_list = worksheet.range(_range)
    idx = 0
    for cell in header_list:
        cell.value = columns[idx]
        idx += 1
    worksheet.update_cells(header_list)

    #constructing values
    _range = get_range_values(len(columns), len(values[0]), _range="A2:")
    values_list = worksheet.range(_range)
    idx = 0
    idy = 0

    for cell in values_list:
        if idx == len(columns):
            idx = 0
            idy += 1
        if idy >= len(values[0]):
            break
        if values[idx][idy] == "NULL":
            cell.value = ""
        else:
            cell.value=values[idx][idy]
        idx += 1
    worksheet.update_cells(values_list)

def get_range_header(_len, _range):
    if _len <= 0:
        print("Error")
    elif _len <= 26:
        _range += alphabet[_len - 1] + '1'
    else:
        _range += 'A'
        _len -= 26
        _range = get_range_header(_len, _range)
    return _range

def get_range_values(col, line, _range):
    string = get_range_header(col, _range)
    string = string[:-1] + str(line + 1)
    return string

if __name__== "__main__":
  main()